<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InputWeeklyCareHours extends Model
{
    protected $table = 'input_weekly_care_hours';
    
    public function budget()
    {
        return $this->belongsTo('App\Budget', 'budget_id');
    }

    public function flat()
    {
        return $this->belongsTo('App\SchemeFlat', 'flat_id');
    }

    public function getCostPerHour()
    {
        return $this->budget->scheme->fpaPurchasedServiceCharges()->where('purchased_service_type_id', $this->purchased_service_type_id)->first()->charge_per_hour;
    }

    public function getTotalCost($expenseCodeID)
    {
        if ($this->purchasedServiceType->expenseCode->id == $expenseCodeID)
        {
            return $this->getCostPerHour() * $this->number_of_hours;
        }
        
        return 0;
    }

    public function purchasedServiceType()
    {
    	return $this->belongsTo('App\PurchasedServiceType', 'purchased_service_type_id');
    }
}
