<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class ExpenseCode extends Model
{
    protected $table = 'expense_codes';

    public function calculatedOverallExpense()
    {
        return $this->hasMany('App\CalculatedOverallExpense', 'expense_code_id');
    }

    public function fpaAnnualSpendActual()
    {
        return $this->hasMany('App\FPAAnnualSpendActual', 'expense_code_id');
    }

    public function fpaAnnualSpendSplit()
    {
        return $this->hasMany('App\FPAAnnualSpendSplit', 'expense_code_id');
    }

    public function fpaWeeklyRegularIncome()
    {
        return $this->hasMany('App\FPAWeeklyRegularIncome', 'expense_code_id');
    }

    public function getActualSpend($natureOfSpend, $schemeID = null)
    {
        if (!$schemeID)
        {
            $schemeID = Auth::user()->myOnlySchemeID();
        }

        return $this->fpaAnnualSpendActual()->where('nature_of_spend', $natureOfSpend)->where('scheme_id', $schemeID)->sum('total_annual_spend');
    }

    //Returns an array of size 12.
    public function getPhaseArray($amount)
    {
        $return = [];
        $percentages = $this->getPhasePercentages($this->phasing_type);

        foreach ($percentages as $id => $percent)
        {
            $return[$id] = number_format($amount * $percent, 2);
        }

        return $return;
    }

    public function getPhasePercentages($phaseType)
    {
        $return = [];

        switch ($phaseType)
        {
            case 'equal' :
                for ($i = 1; $i <= 12; $i++)
                {
                    $return[$i] = 1/12;
                }
                break;
                
            default:
                $return[1]  = 30/365;
                $return[2]  = 31/365;
                $return[3]  = 30/365;
                $return[4]  = 31/365;
                $return[5]  = 31/365;
                $return[6]  = 30/365;
                $return[7]  = 31/365;
                $return[8]  = 30/365;
                $return[9]  = 31/365;
                $return[10]  = 31/365;
                $return[11]  = 28/365;
                $return[12]  = 31/365;
                break;
        }

        return $return;
    }

    public function inputAnnualSpendEstimate()
    {
        return $this->hasMany('App\InputAnnualSpendEstimate', 'expense_code_id');
    }

    public function purchasedServiceType()
    {
        return $this->hasMany('App\PurchasedServiceType', 'expense_code_id');
    }

    public function staffOncostType()
    {
        return $this->belongsTo('App\StaffOncostType', 'staff_oncost_type_id');
    }

    public function staffTypes()
    {
        return $this->belongsToMany('App\StaffType', 'link_expense_codes_staff_types', 'expense_code_id', 'staff_type_id');
    }
}
