<?php

namespace App;

use App\ExpenseCode;
use Auth;
use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
    protected $table = 'budgets';

    public function calculatedOverallExpense()
    {
        return $this->hasMany('App\CalculatedOverallExpense', 'budget_id');
    }

    public function getAnnualSpend($expenseCodeID)
    {
        $row = $this->inputAnnualSpendEstimate()->where('expense_code_id', $expenseCodeID)->first();
        if ($row)
        {
            return $row->total_annual_spend;
        }

        return 0;
    }

    public function getExpenseCodeStreamSplit($expenseCodeID, $businessStreamID = 0)
    {
        $percentage = 1;
        if ($businessStreamID)
        {
            $object = $this->scheme->fpaAnnualSpendSplit()->where('expense_code_id', $expenseCodeID)->where('business_stream_id', $businessStreamID)->first();

            if ($object)
            {
                $percentage = $object->percentage / 100;
            } else {
                $percentage = 0;
            }            
        }

        return $percentage;
    }

    public function getFullPhasedBudget($businessStreamID = 0)
    {
        $expenseCodes = ExpenseCode::all();

        $return = [];
        foreach ($expenseCodes as $expenseCode)
        {
            $return[$expenseCode->id] = $expenseCode->getPhaseArray($this->total($expenseCode->id, $businessStreamID));
        }

        return $return;
    }

    public function getStaffHours($staffTypeID)
    {
        $row = $this->inputStaffHoursTotal()->where('staff_type_id', $staffTypeID)->first();
        if ($row)
        {
            return $row->number_of_hours;
        }

        return 0;
    }

    public function getStaffStreams($staffTypeID, $businessStreamID)
    {
        $row = $this->inputStaffHoursStreamSplit()->where('staff_type_id', $staffTypeID)->where('business_stream_id', $businessStreamID)->first();
        if ($row)
        {
            return $row->percentage;
        }

        return 0;
    }

    public function getWeeklyCareHours($flatID, $purchasedServiceTypeID)
    {
        $row = $this->inputWeeklyCareHours()->where('flat_id', $flatID)->where('purchased_service_type_id', $purchasedServiceTypeID)->first();

        if ($row)
        {
            return $row->number_of_hours;
        }

        return 0;
    }

    public function hasAnnualSpend()
    {
        return count($this->inputAnnualSpendEstimate) > 0;
    }

    public function hasOverrideStaffPay()
    {
        return count($this->scheme->inputOverrideStaffPay) > 0;
    }

    public function hasStaffHours()
    {
        return count($this->inputStaffHoursStreamSplit) > 0;
    }

    public function hasStaffStreams()
    {
        return count($this->inputStaffHoursTotal) > 0;
    }

    public function hasWeeklyCareHours()
    {
        return count($this->inputWeeklyCareHours) > 0;
    }

    public function inputAnnualSpendEstimate()
    {
        return $this->hasMany('App\InputAnnualSpendEstimate', 'budget_id');
    }

    public function inputStaffHoursStreamSplit()
    {
        return $this->hasMany('App\InputStaffHoursStreamSplit', 'budget_id');
    }

    public function inputStaffHoursTotal()
    {
        return $this->hasMany('App\InputStaffHoursTotal', 'budget_id');
    }

    public function inputWeeklyCareHours()
    {
        return $this->hasMany('App\InputWeeklyCareHours', 'budget_id');
    }

    public function isOK()
    {
        $array = $this->isOKArray();

        foreach ($array as $value)
        {
            if (!$value)
            {
                return false;
            }
        }

        return true;
    }

    public function isOKArray()
    {
        $ok['annual_spend'] = $this->hasAnnualSpend();
        //$ok['override_staff_pay'] = $this->hasOverrideStaffPay();
        $ok['staff_hours'] = $this->hasStaffHours();
        $ok['staff_streams'] = $this->hasStaffStreams();
        $ok['weekly_care_hours'] = $this->hasWeeklyCareHours();

        return $ok;
    }

    public function scheme()
    {
        return $this->belongsTo('App\Scheme', 'scheme_id');
    }

    public function signoff()
    {
        $signoff = new BudgetSignoff;
        $signoff->datetime_of_signoff = date('Y-m-d h:i:s');
        $signoff->user_id = Auth::id();
        $signoff->budget_id = $this->id;
        $signoff->save();
    }

    public function signoffs()
    {
        return $this->hasMany('App\BudgetSignoff', 'budget_id');
    }

    public function submit()
    {
        $this->status = 'submitted';
        $this->save();
        $this->signoff();
    }

    public function total($expenseCodeID, $businessStreamID = 0)
    {
        return $this->totalIncome($expenseCodeID, $businessStreamID) 
            + $this->totalOtherCosts($expenseCodeID, $businessStreamID) 
            + $this->totalStaffCosts($expenseCodeID, $businessStreamID);
    }

    public function totalIncome($expenseCodeID, $businessStreamID = 0)
    {
        return $this->totalIncomePurchased($expenseCodeID, $businessStreamID) 
            + $this->totalIncomeWeekly($expenseCodeID, $businessStreamID);
    }

    public function totalIncomePurchased($expenseCodeID, $businessStreamID = 0)
    {
        $return = 0;

        foreach ($this->inputWeeklyCareHours as $input)
        {
            $return += $input->getTotalCost($expenseCodeID);
        }

        return $return * 52 * $this->getExpenseCodeStreamSplit($expenseCodeID, $businessStreamID);
    }

    public function totalIncomeWeekly($expenseCodeID, $businessStreamID = 0)
    {
        return $this->scheme->totalIncomeWeekly($expenseCodeID, $businessStreamID) * 52 * $this->getExpenseCodeStreamSplit($expenseCodeID, $businessStreamID);
    }

    public function totalOtherCosts($expenseCodeID, $businessStreamID = 0)
    {
        $total = $this->inputAnnualSpendEstimate()->where('expense_code_id', $expenseCodeID)->sum('total_annual_spend');

        return - $total * $this->getExpenseCodeStreamSplit($expenseCodeID, $businessStreamID);
    }

    public function totalStaffCosts($expenseCodeID, $businessStreamID = 0)
    {
        $return = 0;

        $hours = $this->inputStaffHoursTotal()->get();

        foreach ($hours as $hour)
        {
            $return += $hour->getTotalCost($expenseCodeID, $businessStreamID);
        }

        return - $return * 52;
    }

    public function updateAnnualSpendEstimate($expenseCodeID, $estimate)
    {
        $annualSpend = $this->inputAnnualSpendEstimate()->where('expense_code_id', $expenseCodeID)->first();
        if ($annualSpend)
        {
            $annualSpend->total_annual_spend = $estimate;
            $annualSpend->save();
        } else {
            $annualSpend = new InputAnnualSpendEstimate;
            $annualSpend->budget_id = $this->id;
            $annualSpend->scheme_id = $this->scheme_id;
            $annualSpend->expense_code_id = $expenseCodeID;
            $annualSpend->total_annual_spend = $estimate;
            $annualSpend->save();
        }
    }

    public function updateStaffHoursEstimate($staffTypeID, $estimate)
    {
        $annualSpend = $this->inputStaffHoursTotal()->where('staff_type_id', $staffTypeID)->first();
        if ($annualSpend)
        {
            $annualSpend->number_of_hours = $estimate;
            $annualSpend->save();
        } else {
            $annualSpend = new inputStaffHoursTotal;
            $annualSpend->budget_id = $this->id;
            $annualSpend->scheme_id = $this->scheme_id;
            $annualSpend->staff_type_id = $staffTypeID;
            $annualSpend->number_of_hours = $estimate;
            $annualSpend->save();
        }
    }

    public function updateStaffStreamSplit($staffTypeID, $businessStreamID, $estimate)
    {
        $annualSpend = $this->inputStaffHoursStreamSplit()->where('staff_type_id', $staffTypeID)->where('business_stream_id', $businessStreamID)->first();
        if ($annualSpend)
        {
            $annualSpend->percentage = $estimate;
            $annualSpend->save();
        } else {
            $annualSpend = new inputStaffHoursStreamSplit;
            $annualSpend->budget_id = $this->id;
            $annualSpend->scheme_id = $this->scheme_id;
            $annualSpend->staff_type_id = $staffTypeID;
            $annualSpend->business_stream_id = $businessStreamID;
            $annualSpend->percentage = $estimate;
            $annualSpend->save();
        }
    }

    public function updateWeeklyCareHoursEstimate($flatID, $purchasedServiceTypeID, $estimate)
    {
        if (!$estimate) $estimate = 0;

        $annualSpend = $this->inputWeeklyCareHours()->where('flat_id', $flatID)->where('purchased_service_type_id', $purchasedServiceTypeID)->first();
        if ($annualSpend)
        {
            $annualSpend->number_of_hours = $estimate;
            $annualSpend->save();
        } else {
            $annualSpend = new inputWeeklyCareHours;
            $annualSpend->budget_id = $this->id;
            $annualSpend->purchased_service_type_id = $purchasedServiceTypeID;
            $annualSpend->flat_id = $flatID;
            $annualSpend->number_of_hours = $estimate;
            $annualSpend->save();
        }
    }
}
