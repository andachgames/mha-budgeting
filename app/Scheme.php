<?php

namespace App;

use App\Budget;
use Illuminate\Database\Eloquent\Model;

class Scheme extends Model
{
    protected $table = 'schemes';

    public function area()
    {
        return $this->hasMany('App\SchemeArea', 'area_id');
    }

    public function budgets()
    {
        return $this->hasMany('App\Budget', 'scheme_id');
    }

    public function calculatedOverallExpense()
    {
        return $this->hasMany('App\CalculatedOverallExpense', 'scheme_id');
    }

    public function currentEditableBudget()
    {
        if (count($this->budgets->where('status', 'editable')) == 0)
        {
            $budget = new Budget;
            $budget->scheme_id = $this->id;
            $budget->status = 'editable';
            $budget->save();
            return $budget;
        }

        return $this->budgets()->where('status', 'editable')->first();
    }

    public function flats()
    {
        return $this->hasMany('App\SchemeFlat', 'scheme_id');
    }

    public function fpaAnnualSpendActual()
    {
        return $this->hasMany('App\FPAAnnualSpendActual', 'scheme_id');
    }

    public function fpaAnnualSpendSplit()
    {
        return $this->hasMany('App\FPAAnnualSpendSplit', 'scheme_id');
    }

    public function fpaPurchasedServiceCharges()
    {
        return $this->hasMany('App\FPAPurchasedServiceCharge', 'scheme_id');
    }

    public function fpaWeeklyRegularIncome()
    {
        return $this->hasManyThrough('App\FPAWeeklyRegularIncome', 'App\SchemeFlat', 'scheme_id', 'flat_id');
    }

    public function inputAnnualSpendEstimate()
    {
        return $this->hasMany('App\InputAnnualSpendEstimate', 'scheme_id');
    }

    public function inputOverrideStaffPay()
    {
        return $this->hasMany('App\InputOverrideStaffPay', 'scheme_id');
    }

    public function inputStaffHoursStreamSplit()
    {
        return $this->hasMany('App\InputStaffHoursStreamSplit', 'scheme_id');
    }

    public function staffOncostPercentages()
    {
        return $this->hasMany('App\StaffOncostPercentage', 'scheme_id');
    }

    public function totalIncomeWeekly($expenseCodeID, $businessStreamID)
    {
        if ($businessStreamID)
        {
            return $this->fpaWeeklyRegularIncome()->where('expense_code_id', $expenseCodeID)->where('business_stream_id', $businessStreamID)->sum('weekly_income');
        }
        return $this->fpaWeeklyRegularIncome()->where('expense_code_id', $expenseCodeID)->sum('weekly_income');
    }

    public function users()
    {
        return $this->hasMany('App\User', 'link_schemes_users', 'scheme_id', 'user_id');
    }
}
