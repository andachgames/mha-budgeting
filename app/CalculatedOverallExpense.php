<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalculatedOverallExpense extends Model
{
    protected $table = 'calculated_overall_expense';

    public function budget()
    {
        return $this->belongsTo('App\Budget', 'budget_id');
    }

    public function businessStream()
    {
        return $this->belongsTo('App\BusinessStream', 'business_stream_id');
    }

    public function expenseCode()
    {
        return $this->belongsTo('App\ExpenseCode', 'expense_code_id');
    }

    public function scheme()
    {
        return $this->belongsTo('App\Scheme', 'scheme_id');
    }
}
