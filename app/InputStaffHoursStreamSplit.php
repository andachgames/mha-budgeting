<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InputStaffHoursStreamSplit extends Model
{
    protected $table = 'input_staff_hours_stream_split';

    public function budget()
    {
        return $this->belongsTo('App\Budget', 'budget_id');
    }

    public function businessStream()
    {
        return $this->belongsTo('App\BusinessStream', 'business_stream_id');
    }

    public function scheme()
    {
        return $this->belongsTo('App\Scheme', 'scheme_id');
    }

    public function staffType()
    {
        return $this->belongsTo('App\StaffType', 'staff_type_id');
    }
}
