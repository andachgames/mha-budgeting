<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchemeFlat extends Model
{
    protected $table = 'schemes_flats';

    public function fpaWeeklyRegularIncome()
    {
        return $this->hasMany('App\FPAWeeklyRegularIncome', 'flat_id');
    }

    public function inputWeeklyCareHours()
    {
        return $this->hasMany('App\InputWeeklyCareHours', 'flat_id');
    }

    public function scheme()
    {
        return $this->belongsTo('App\Scheme', 'scheme_id');
    }
}
