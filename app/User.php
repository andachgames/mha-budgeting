<?php

namespace App;

use App\Budget;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function currentEditableBudget()
    {
        if (count($this->schemes) == 1)
        {
            $scheme = $this->schemes->first();

            return $scheme->currentEditableBudget();
        }

        return false;
    }

    public function myBudgets()
    {
        $array = [];
        foreach ($this->schemes as $scheme)
        {
            foreach ($scheme->budgets as $budget)
            {
                $array[$budget->id] = 1;
            }
        }

        return Budget::whereIn('id', array_flip($array));
    }

    public function myOnlyScheme()
    {
        if (count($this->schemes) == 1)
        {
            return $this->schemes->first();
        }

        return null;
    }

    public function myOnlySchemesFlats()
    {
        if (count($this->schemes) == 1)
        {
            return $this->schemes->first()->flats;
        }

        return null;
    }

    public function myOnlySchemeID()
    {
        if (count($this->schemes) == 1)
        {
            return $this->schemes->first()->id;
        }

        return null;
    }

    public function schemes()
    {
        return $this->belongsToMany('App\Scheme', 'link_schemes_users', 'user_id', 'scheme_id');
    }

    public function signoffs()
    {
        return $this->hasMany('App\BudgetSignoff', 'user_id');
    }

    public function submitBudget($budget)
    {
        if (!$this->schemes->contains($budget->scheme_id))
        {
            return false;
        }

        $budget->submit();

        return true;
    }
}
