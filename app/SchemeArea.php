<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchemeArea extends Model
{
    protected $table = 'schemes_areas';

    public function fpaStaffPayRates()
    {
        return $this->hasMany('App\FPAStaffPayRate', 'area_id');
    }

    public function scheme()
    {
    	return $this->hasMany('App\Scheme', 'area_id');
    }
}
