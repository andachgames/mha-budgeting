<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchasedServiceType extends Model
{
    protected $table = 'purchased_service_types';

    public function expenseCode()
    {
    	return $this->belongsTo('App\ExpenseCode', 'expense_code_id');
    }

    public function fpaPurchasedServiceCharges()
    {
    	return $this->hasMany('App\FPAPurchasedServiceCharges', 'purchased_service_type_id');
    }
}
