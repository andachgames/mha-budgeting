<?php

namespace App\Http\Controllers;

use App\Budget;
use App\BusinessStream;
use App\ExpenseCode;
use App\FPAStaffPayRate;
use App\StaffType;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth')->except('index');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function budget($id, $businessStreamID = 0)
    {
        $budget = Budget::find($id);
        $expenseCodes = ExpenseCode::all();
        $stream = '';
        if ($businessStreamID)
        {
            $stream = BusinessStream::find($businessStreamID);
        }

        $streams = BusinessStream::all();

        return view('main.budget', ['budget' => $budget, 'expenseCodes' => $expenseCodes, 'businessStreamID' => $businessStreamID, 'stream' => $stream, 'streams' => $streams]);
    }

    public function budgets()
    {
        $budgets = Auth::user()->myBudgets();
        
        return view('main.budgets', ['budgets' => $budgets]);
    }

    public function input()
    {
        $budget = Auth::user()->currentEditableBudget();
        $ok = $budget->isOKArray();

        return view('main.input', ['budget' => $budget, 'ok' => $ok]);
    }

    public function inputAnnualSpend()
    {
        $budget = Auth::user()->currentEditableBudget();
        $expenseCodes = ExpenseCode::all();

        $currentEstimate = [];
        foreach ($expenseCodes as $expenseCode)
        {
            $currentEstimate[$expenseCode->id] = $budget->getAnnualSpend($expenseCode->id);
        }

        return view('input.annualspend', ['expenseCodes' => $expenseCodes, 'currentEstimate' => $currentEstimate]);
    }

    public function inputAnnualSpendPost(Request $request)
    {
        $budget = Auth::user()->currentEditableBudget();
        $estimates = $request->spend;

        foreach ($estimates as $expenseCodeID => $estimate)
        {
            $budget->updateAnnualSpendEstimate($expenseCodeID, $estimate);
        }

        session()->flash('success', 'Annual Spend Estimates have been updated');

        return redirect()->route('input-annual-spend');
    }

    public function inputOverrideStaffPay()
    {
        $staffTypes = StaffType::all();

        return view('input.overridestaffpay', ['staffTypes' => $staffTypes]);
    }

    public function inputOverrideStaffPayPost(Request $request)
    {
        dd($request);

        return redirect()->route('input-override-staff-pay');
    }

    public function inputStaffHours()
    {
        $budget = Auth::user()->currentEditableBudget();
        $staffTypes = StaffType::all();

        $currentEstimate = [];
        foreach ($staffTypes as $staffType)
        {
            $currentEstimate[$staffType->id] = $budget->getStaffHours($staffType->id);
        }

        return view('input.staffhours', ['budget' => $budget, 'staffTypes' => $staffTypes, 'currentEstimate' => $currentEstimate]);
    }

    public function inputStaffHoursPost(Request $request)
    {
        $budget = Auth::user()->currentEditableBudget();
        $estimates = $request->hours;

        foreach ($estimates as $staffTypeID => $hours)
        {
            $budget->updateStaffHoursEstimate($staffTypeID, $hours);
        }

        session()->flash('success', 'Annual Spend Estimates have been updated');

        return redirect()->route('input-staff-hours');
    }

    public function inputStaffStreams()
    {
        $budget = Auth::user()->currentEditableBudget();
        $staffTypes = StaffType::all();
        $businessStreams = BusinessStream::all();

        $currentEstimate = [];
        foreach ($staffTypes as $staffType)
        {
            foreach ($businessStreams as $stream)
            {
                $currentEstimate[$staffType->id][$stream->id] = $budget->getStaffStreams($staffType->id, $stream->id);
            }
        }

        return view('input.staffstreams', ['budget' => $budget, 'businessStreams' => $businessStreams, 'staffTypes' => $staffTypes, 'currentEstimate' => $currentEstimate]);
    }

    public function inputStaffStreamsPost(Request $request)
    {
        $budget = Auth::user()->currentEditableBudget();
        $estimates = $request->percentage;

        foreach ($estimates as $staffTypeID => $array)
        {
            foreach ($array as $businessStreamID => $percentage)
            {
                $budget->updateStaffStreamSplit($staffTypeID, $businessStreamID, $percentage);
            }
        }

        session()->flash('success', 'Staff Stream Split Estimates have been updated');

        return redirect()->route('input-staff-streams');
    }

    public function inputWeeklyCareHours()
    {
        $budget = Auth::user()->currentEditableBudget();
        $flats = Auth::user()->myOnlySchemesFlats();
        $scheme = Auth::user()->myOnlyScheme();
        $chargeForCare = $scheme->fpaPurchasedServiceCharges->where('purchased_service_type_id', 1)->first()->charge_per_hour;
        $chargeForDomestic = $scheme->fpaPurchasedServiceCharges->where('purchased_service_type_id', 2)->first()->charge_per_hour;

        $currentEstimate = [];
        foreach ($flats as $flat)
        {
            $currentEstimate[$flat->id]['care'] = $budget->getWeeklyCareHours($flat->id, 1);
            $currentEstimate[$flat->id]['domestic'] = $budget->getWeeklyCareHours($flat->id, 2);
        }

        return view('input.weeklycarehours', ['flats' => $flats, 'chargeForCare' => $chargeForCare, 'chargeForDomestic' => $chargeForDomestic, 'currentEstimate' => $currentEstimate]);
    }

    public function inputWeeklyCareHoursPost(Request $request)
    {
        $budget = Auth::user()->currentEditableBudget();
        $estimatesCare = $request->care;
        $estimatesDomestic = $request->domestic;

        foreach ($estimatesCare as $flatID => $hours)
        {
            $budget->updateWeeklyCareHoursEstimate($flatID, 1, $hours);
        }

        foreach ($estimatesDomestic as $flatID => $hours)
        {
            $budget->updateWeeklyCareHoursEstimate($flatID, 2, $hours);
        }

        session()->flash('success', 'Care/Domestic Hour Estimates have been updated');

        return redirect()->route('input-weekly-care-hours');
    }

    public function phasedBudget($id, $businessStreamID = 0)
    {
        $budget = Budget::find($id);
        $expenseCodes = ExpenseCode::all();
        $stream = '';
        if ($businessStreamID)
        {
            $stream = BusinessStream::find($businessStreamID);
        }

        $streams = BusinessStream::all();

        $phasedBudget = $budget->getFullPhasedBudget($businessStreamID);

        return view('main.phasedbudget', ['budget' => $budget, 'expenseCodes' => $expenseCodes, 'businessStreamID' => $businessStreamID, 'stream' => $stream, 'streams' => $streams, 'phasedBudget' => $phasedBudget]);
    }

    public function staffCosts($schemeID = null)
    {
        $scheme = Auth::user()->myOnlyScheme();
        $staffTypes = StaffType::all();

        return view('main.staffcosts', ['scheme' => $scheme, 'staffTypes' => $staffTypes]);
    }

    public function submit()
    {
        $budget = Auth::user()->currentEditableBudget();
        $isOK = $budget->isOK();
        $isOKArray = $budget->isOKArray();

        return view('main.submit', ['budget' => $budget, 'isOK' => $isOK, 'isOKArray' => $isOKArray]);
    }

    public function submitPost(Request $request)
    {
        $budget = Budget::find($request->budgetID);
        if (Auth::user()->submitBudget($budget))
        {
            session()->flash('success', 'The budget has been submitted. Thankyou');
        } else {
            session()->flash('error', 'There was an error submitting this budget.');
        }

        return redirect()->route('home');
    }
}
