<?php

namespace App;

use App\InputWeeklyCareHours;
use Auth;
use Form;
use Illuminate\Database\Eloquent\Model;

class StaffType extends Model
{
    protected $table = 'staff_types';

    public function businessStreams()
    {
        return $this->belongsToMany('App\BusinessStream', 'link_business_streams_staff_types', 'staff_type_id', 'business_stream_id');
    }

    public function expenseCodes()
    {
        return $this->belongsToMany('App\ExpenseCode', 'link_expense_codes_staff_types', 'staff_type_id', 'expense_code_id');
    }

    public function getAdjustedPay($schemeID)
    {
        return $this->getPayRate($schemeID) * $this->getTotalOncostToMultiply($schemeID);
    }

    public function getCalculatedNumberOfHours()
    {
        $return = 0;

        switch ($this->special_calculation_method)
        {
            case 'care_contract' :
                return Auth::user()->myOnlyScheme()->contract_care_hours_total;
            break;

            case 'care_purchased_services' :
                $budget = Auth::user()->currentEditableBudget();

                return InputWeeklyCareHours::where('budget_id', $budget->id)->where('purchased_service_type_id', 1)->sum('number_of_hours');
            break;

            case 'domestic_assisted_living' :
                return Auth::user()->myOnlyScheme()->flats()->sum('assisted_living_hours_per_week');
            break;

            case 'domestic_contract' :
                return Auth::user()->myOnlyScheme()->contract_domestic_hours_total;
            break;

            case 'domestic_purchased_services' :
                $budget = Auth::user()->currentEditableBudget();

                return InputWeeklyCareHours::where('budget_id', $budget->id)->where('purchased_service_type_id', 2)->sum('number_of_hours');
            break;
        }

        return $return;
    }

    public function getFormattedAdjustedPayPerHour($schemeID)
    {
        return number_format($this->getAdjustedPay($schemeID), 2);
    }

    public function getFormattedPayPerHour($schemeID)
    {
        return number_format($this->getPayRate($schemeID), 2);
    }

    public function getHoursFormInput($value, $name = '')
    {
        $options = ['class' => 'form-control'];
        if (!$name) 
        {
            $name = 'hours['.$this->id.']';
        }

        //Sometimes the value is overwritten
        if ($var = $this->getCalculatedNumberOfHours())
        {
            $value = $var;
        }

        if ($this->special_calculation_method)
        {
            $options['disabled'] = 'disabled';
        }

        return Form::text($name, $value, $options);
    }

    public function getOncost($oncostID, $schemeID)
    {
        $object = $this->staffOncostPercentages()->where('scheme_id', $schemeID)->where('staff_oncost_type_id', $oncostID)->first();

        if ($object)
        {
            return number_format($object->percentage, 1);
        }
        return number_format(0, 1);
    }

    public function getOncostToMultiply($oncostID, $schemeID)
    {
        return 1 + $this->getOncost($oncostID, $schemeID) / 100;
    }
    
    public function getPayRate($schemeID)
    {
        //First we have to check whether we have an override entered. 
        $object = $this->inputOverrideStaffPay()->where('scheme_id', $schemeID)->first();
        if ($object)
        {
            return $object->overridden_hourly_pay;
        }

        $areaID = Scheme::find($schemeID)->area_id;
        return $this->fpaStaffPayRates()->where('area_id', $areaID)->first()->pay_rate;
    }

    public function getStreamFormInput($value, $businessStreamID, $name = '')
    {
        $options = ['class' => 'form-control'];
        if (!$name) 
        {
            $name = 'percentage['.$this->id.']['.$businessStreamID.']';
        }

        if (!$this->businessStreams->contains($businessStreamID))
        {
            $options['disabled'] = 'disabled';
        }

        return Form::text($name, $value, $options);
    }

    public function getTotalOncostToMultiply($schemeID)
    {
        return $this->getOncostToMultiply(1, $schemeID) * 
            $this->getOncostToMultiply(2, $schemeID) * 
            $this->getOncostToMultiply(3, $schemeID) * 
            $this->getOncostToMultiply(4, $schemeID) * 
            $this->getOncostToMultiply(5, $schemeID);
    }

    public function fpaStaffPayRates()
    {
        return $this->hasMany('App\FPAStaffPayRate', 'staff_type_id');
    }

    public function inputOverrideStaffPay()
    {
        return $this->hasMany('App\InputOverrideStaffPay', 'staff_type_id');
    }

    public function inputStaffHours()
    {
        return $this->hasMany('App\InputStaffHoursTotal', 'staff_type_id');
    }

    public function inputStaffStreams()
    {
        return $this->hasMany('App\InputStaffHoursStreamSplit', 'staff_type_id');
    }

    public function staffOncostPercentages()
    {
        return $this->hasMany('App\StaffOncostPercentage', 'staff_type_id');
    }
}
