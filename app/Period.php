<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Period extends Model
{
    protected $table = 'periods';

    public function phasedOverallExpense()
    {
        return $this->hasMany('App\PhasedOverallExpense', 'period_id');
    }
}
