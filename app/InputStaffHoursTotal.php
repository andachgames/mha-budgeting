<?php

namespace App;

use App\ExpenseCode;
use App\FPAStaffPayRate;
use App\InputStaffHoursStreamSplit;
use App\StaffOncostPercentage;
use Illuminate\Database\Eloquent\Model;

class InputStaffHoursTotal extends Model
{
    protected $table = 'input_staff_hours_total';
    
    public function budget()
    {
        return $this->belongsTo('App\Budget', 'budget_id');
    }

    public function getBaseCostPerHour()
    {
        return $this->staffType->getPayRate($this->scheme_id);
    }

    public function getPercentageForBusinessStream($businessStreamID = 0)
    {
        $percentageForBusinessStream = 1;
        if ($businessStreamID)
        {
            $percentageForBusinessStream = InputStaffHoursStreamSplit::where('business_stream_id', $businessStreamID)->where('area_id', $this->scheme->area_id)->where('staff_type_id', $this->staff_type_id)->first()->percentage / 100;
        }

        return $percentageForBusinessStream;
    }

    public function getOncostPercentage($staffOncostTypeID)
    {
        $object = StaffOncostPercentage::where('scheme_id', $this->scheme_id)->where('staff_type_id', $this->staff_type_id)->where('staff_oncost_type_id', $staffOncostTypeID)->first();

        if (!$object) 
        {
            return 0;
        }

        return $object->percentage / 100;
    }

    public function getTotalCost($expenseCodeID, $businessStreamID = 0)
    {
        if (!$this->number_of_hours) return 0;

        $expenseCode = ExpenseCode::find($expenseCodeID);
        if (!$expenseCode->staffTypes->contains($this->id))
        {
            return 0;
        }

        $costPerHour = $this->getBaseCostPerHour();
        $percentageForBusinessStream = $this->getPercentageForBusinessStream();
        $oncostPercentage = 1;

        //If the expense code is linked to a staff oncost type, then take the appropriate percentage. 
        if ($expenseCode->staff_oncost_type_id)
        {
            $oncostPercentage = $this->getOncostPercentage($expenseCode->staff_oncost_type_id);
        }

        return $this->number_of_hours * $costPerHour * $percentageForBusinessStream * $oncostPercentage;
    }

    public function scheme()
    {
        return $this->belongsTo('App\Scheme', 'scheme_id');
    }

    public function staffType()
    {
        return $this->belongsTo('App\StaffType', 'staff_type_id');
    }
}
