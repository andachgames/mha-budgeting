<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhasedOverallExpense extends Model
{
    protected $table = 'phased_overall_expense';

    public function budget()
    {
        return $this->belongsTo('App\Budget', 'budget_id');
    }

    public function calculatedOverallExpense()
    {
        return $this->belongTo('App\CalculatedOverallExpense', 'calculated_expense_id');
    }
}
