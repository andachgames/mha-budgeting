<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FPAAnnualSpendSplit extends Model
{
    protected $table = 'fpa_annual_spend_split';

    public function businessStream()
    {
    	return $this->belongsTo('App\BusinessStream', 'business_stream_id');
    }
    
    public function expenseCode()
    {
        return $this->belongsTo('App\ExpenseCode', 'expense_code_id');
    }

    public function scheme()
    {
        return $this->belongsTo('App\Scheme', 'scheme_id');
    }
}
