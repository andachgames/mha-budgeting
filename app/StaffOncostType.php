<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffOncostType extends Model
{
    protected $table = 'staff_oncost_types';

    public function expenseCode()
    {
    	return $this->hasMany('App\ExpenseCode', 'staff_oncost_type_id');
    }

    public function staffOncostPercentages()
    {
        return $this->hasMany('App\StaffOncostPercentage', 'staff_oncost_type_id');
    }
}
