<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaffOncostPercentage extends Model
{
    protected $table = 'staff_oncost_percentages';

    public function scheme()
    {
    	return $this->belongsTo('App\Scheme', 'scheme_id');
    }

    public function staffOncostType()
    {
    	return $this->belongsTo('App\StaffOncostType', 'staff_oncost_type_id');
    }

    public function staffType()
    {
    	return $this->belongsTo('App\StaffType', 'staff_type_id');
    }
}
