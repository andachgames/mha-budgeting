<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BudgetSignoff extends Model
{
    protected $table = 'budgets_signoff';

    public function budget()
    {
        return $this->belongsTo('App\Budget', 'budget_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
