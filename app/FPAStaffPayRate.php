<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FPAStaffPayRate extends Model
{
    protected $table = 'fpa_staff_pay_rates';

    public function area()
    {
    	return $this->belongsTo('App\SchemeArea', 'area_id');
    }

    public function staffType()
    {
    	return $this->belongsTo('App\StaffType', 'staff_type_id');
    }
}
