<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FPAWeeklyRegularIncome extends Model
{
    protected $table = 'fpa_weekly_regular_income';

    public function businessStream()
    {
    	return $this->belongsTo('App\BusinessStream', 'business_stream_id');
    }

    public function expenseCode()
    {
        return $this->belongsTo('App\ExpenseCode', 'expense_code_id');
    }

    public function flat()
    {
        return $this->belongsTo('App\SchemeFlat', 'scheme_id');
    }
}
