<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusinessStream extends Model
{
    protected $table = 'business_streams';
    
    public function calculatedOverallExpense()
    {
        return $this->hasMany('App\CalculatedOverallExpense', 'business_stream_id');
    }

    public function fpaAnnualSpendSplit()
    {
        return $this->hasMany('App\FPAAnnualSpendSplit', 'business_stream_id');
    }

    public function fpaWeeklyRegularIncome()
    {
        return $this->hasMany('App\FPAWeeklyRegularIncome', 'business_stream_id');
    }

    public function inputStaffHoursStreamSplit()
    {
        return $this->hasMany('App\InputStaffHoursStreamSplit', 'business_stream_id');
    }

    public function staffTypes()
    {
        return $this->belongsToMany('App\StaffType', 'link_business_streams_staff_types', 'business_stream_id', 'staff_type_id');
    }
}
