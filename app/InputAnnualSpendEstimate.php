<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InputAnnualSpendEstimate extends Model
{
    protected $table = 'input_annual_spend_estimate';
    
    public function budget()
    {
        return $this->belongsTo('App\Budget', 'budget_id');
    }

    public function expenseCode()
    {
        return $this->belongsTo('App\ExpenseCode', 'expense_code_id');
    }

    public function scheme()
    {
        return $this->belongsTo('App\Scheme', 'scheme_id');
    }
}
