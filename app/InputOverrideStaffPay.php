<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InputOverrideStaffPay extends Model
{
    protected $table = 'input_override_staff_pay';

    public function scheme()
    {
        return $this->belongsTo('App\Scheme', 'scheme_id');
    }

    public function staffType()
    {
        return $this->belongsTo('App\StaffType', 'staff_type_id');
    }
}
