<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FPAPurchasedServiceCharge extends Model
{
    protected $table = 'fpa_purchased_service_charges';

    public function purchasedServiceType()
    {
        return $this->belongsTo('App\PurchasedServiceType', 'purchased_service_type_id');
    }

    public function scheme()
    {
    	return $this->belongsTo('App\Scheme', 'scheme_id');
    }
}
