<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FPAAnnualSpendActual extends Model
{
    protected $table = 'fpa_annual_spend_actual';
    
    public function expenseCode()
    {
        return $this->belongsTo('App\ExpenseCode', 'expense_code_id');
    }

    public function scheme()
    {
        return $this->belongsTo('App\Scheme', 'scheme_id');
    }
}
