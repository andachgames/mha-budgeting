<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFpaSpend extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fpa_annual_spend_actual', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('total_annual_spend', 8, 2);
            $table->string('nature_of_spend');
            $table->integer('scheme_id');
            $table->integer('expense_code_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fpa_annual_spend_actual');
    }
}
