<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePhasingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expense_codes', function (Blueprint $table) {
            $table->string('phasing_type')->after('staff_oncost_type_id');
        });

        Schema::dropIfExists('fpa_phasing');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expense_codes', function (Blueprint $table) {
            $table->dropColumn('phasing_type');
        });

        Schema::create('fpa_phasing', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('percentage', 8, 2);
            $table->integer('expense_code_id');
            $table->integer('period_id');
            $table->timestamps();
        });
    }
}
