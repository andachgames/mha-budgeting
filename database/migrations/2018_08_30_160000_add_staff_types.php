<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStaffTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('input_staff_hours_stream_split', function (Blueprint $table) {
            $table->string('staff_type_id')->after('percentage');
            $table->dropColumn('expense_code_id');
        });

        Schema::table('input_staff_hours_total', function (Blueprint $table) {
            $table->string('staff_type_id')->after('number_of_hours');
            $table->dropColumn('expense_code_id');
        });

        Schema::create('link_business_streams_staff_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('business_stream_id');
            $table->integer('staff_type_id');
            $table->timestamps();
        });

        Schema::create('staff_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('expense_code_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('input_staff_hours_stream_split', function (Blueprint $table) {
            $table->string('expense_code_id')->after('percentage');
            $table->dropColumn('staff_type_id');
        });

        Schema::table('input_staff_hours_total', function (Blueprint $table) {
            $table->string('expense_code_id')->after('number_of_hours');
            $table->dropColumn('staff_type_id');
        });

        Schema::dropIfExists('link_business_streams_staff_types');
        Schema::dropIfExists('staff_types');
    }
}
