<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPurchasedServicesTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fpa_hourly_care_rates', function (Blueprint $table) {
            $table->string('purchased_service_type_id')->after('scheme_id');
            $table->dropColumn('expense_code_id');
        });

        Schema::create('fpa_purchased_service_charges', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('charge_per_hour', 8, 2);
            $table->integer('purchased_service_type_id');
            $table->integer('scheme_id');
            $table->timestamps();
        });

        Schema::table('input_weekly_care_hours', function (Blueprint $table) {
            $table->string('purchased_service_type_id')->after('flat_id');
        });

        Schema::create('purchased_service_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('expense_code_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fpa_hourly_care_rates', function (Blueprint $table) {
            $table->string('expense_code_id')->after('scheme_id');
            $table->dropColumn('purchased_service_type_id');
        });
        
        Schema::dropIfExists('fpa_purchased_service_charges');

        Schema::table('input_weekly_care_hours', function (Blueprint $table) {
            $table->dropColumn('purchased_service_type_id');
        });

        Schema::dropIfExists('purchased_service_types');
    }
}
