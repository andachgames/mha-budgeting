<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StaffInstructions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('staff_types', function (Blueprint $table) {
            $table->string('instructions_hours')->after('special_calculation_method');
            $table->string('instructions_streams')->after('instructions_hours');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('staff_types', function (Blueprint $table) {
            $table->dropColumn('instructions_hours');
            $table->dropColumn('instructions_streams');
        });
    }
}
