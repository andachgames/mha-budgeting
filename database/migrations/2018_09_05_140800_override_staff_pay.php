<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OverrideStaffPay extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('input_override_staff_pay', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('overridden_hourly_pay', 8, 2);
            $table->integer('scheme_id');
            $table->integer('staff_type_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('input_override_staff_pay');
    }
}
