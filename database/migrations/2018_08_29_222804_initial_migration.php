<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budgets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scheme_id');
            $table->timestamps();
        });

        Schema::create('budgets_signoff', function (Blueprint $table) {
            $table->increments('id');
            $table->datetime('datetime_of_signoff');
            $table->integer('user_id');
            $table->integer('budget_id');
            $table->timestamps();
        });

        Schema::create('business_streams', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('code');
            $table->timestamps();
        });

        Schema::create('calculated_overall_expense', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('calculated_expense', 8, 2);
            $table->integer('budget_id');
            $table->integer('business_stream_id');
            $table->integer('scheme_id');
            $table->integer('expense_code_id');
            $table->timestamps();
        });

        Schema::create('expense_codes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('fpa_hourly_care_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expense_code_id');
            $table->integer('scheme_id');
            $table->decimal('hourly_care_rate', 8, 2);
            $table->timestamps();
        });

        Schema::create('fpa_phasing', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('percentage', 8, 2);
            $table->integer('expense_code_id');
            $table->integer('period_id');
            $table->timestamps();
        });

        Schema::create('fpa_weekly_regular_income', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expense_code_id');
            $table->integer('flat_id');
            $table->decimal('weekly_income', 8, 2);
            $table->timestamps();
        });

        Schema::create('input_annual_spend_estimate', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('total_annual_spend', 8, 2);
            $table->integer('budget_id');
            $table->integer('scheme_id');
            $table->integer('expense_code_id');
            $table->timestamps();
        });

        Schema::create('input_staff_hours_stream_split', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('percentage', 8, 2);
            $table->integer('budget_id');
            $table->integer('business_stream_id');
            $table->integer('scheme_id');
            $table->integer('expense_code_id');
            $table->timestamps();
        });

        Schema::create('input_staff_hours_total', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('number_of_hours', 8, 2);
            $table->integer('budget_id');
            $table->integer('scheme_id');
            $table->integer('expense_code_id');
            $table->timestamps();
        });

        Schema::create('link_schemes_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('scheme_id');
            $table->integer('user_id');
            $table->timestamps();
        });

        Schema::create('periods', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('phased_overall_expense', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('phased_expense', 8, 2);
            $table->integer('period_id');
            $table->integer('calculated_expense_id');
            $table->timestamps();
        });

        Schema::create('schemes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('schemes_flats', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('scheme_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budgets');
        Schema::dropIfExists('budgets_signoff');
        Schema::dropIfExists('business_streams');
        Schema::dropIfExists('calculated_overall_expense');
        Schema::dropIfExists('expense_codes');
        Schema::dropIfExists('fpa_hourly_care_rates');
        Schema::dropIfExists('fpa_phasing');
        Schema::dropIfExists('fpa_weekly_regular_income');
        Schema::dropIfExists('input_annual_spend_estimate');
        Schema::dropIfExists('input_staff_hours_stream_split');
        Schema::dropIfExists('input_staff_hours_total');
        Schema::dropIfExists('link_schemes_users');
        Schema::dropIfExists('periods');
        Schema::dropIfExists('phased_overall_expense');
        Schema::dropIfExists('schemes');
        Schema::dropIfExists('schemes_flats');
    }
}
