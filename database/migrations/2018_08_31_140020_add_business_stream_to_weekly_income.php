<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBusinessStreamToWeeklyIncome extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fpa_weekly_regular_income', function (Blueprint $table) {
            $table->string('business_stream_id')->after('expense_code_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fpa_weekly_regular_income', function (Blueprint $table) {
            $table->dropColumn('business_stream_id');
        });
    }
}
