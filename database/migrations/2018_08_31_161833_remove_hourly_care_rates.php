<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveHourlyCareRates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('fpa_hourly_care_rates');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('fpa_hourly_care_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expense_code_id');
            $table->integer('scheme_id');
            $table->decimal('hourly_care_rate', 8, 2);
            $table->timestamps();
        });
    }
}
