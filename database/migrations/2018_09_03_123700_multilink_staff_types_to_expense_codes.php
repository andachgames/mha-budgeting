<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MultilinkStaffTypesToExpenseCodes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('link_expense_codes_staff_types', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('expense_code_id');
            $table->integer('staff_type_id');
            $table->timestamps();
        });

        Schema::table('staff_types', function (Blueprint $table) {
            $table->dropColumn('expense_code_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('link_expense_codes_staff_types');

        Schema::table('staff_types', function (Blueprint $table) {
            $table->integer('expense_code_id')->after('name')->nullable();
        });
    }
}
