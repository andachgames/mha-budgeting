<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsFromJenny extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expense_codes', function (Blueprint $table) {
            $table->integer('staff_oncost_type_id')->after('name')->nullable();
        });

        Schema::table('fpa_staff_pay_rates', function (Blueprint $table) {
            $table->integer('area_id')->after('pay_rate');
            $table->dropColumn('scheme_id');
        });

        Schema::table('schemes', function (Blueprint $table) {
            $table->string('accounting_type')->after('name');
            $table->integer('contract_care_hours_total')->after('accounting_type')->nullable();
            $table->integer('contract_domestic_hours_total')->after('accounting_type')->nullable();
            $table->integer('area_id')->after('name');
        });

        Schema::create('schemes_areas', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::table('schemes_flats', function (Blueprint $table) {
            $table->string('lease_type')->after('name');
            $table->integer('assisted_living_hours_per_week')->after('lease_type')->nullable();
        });

        Schema::create('staff_oncost_percentages', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('percentage', 8, 2);
            $table->integer('scheme_id')->nullable();
            $table->integer('staff_type_id');
            $table->integer('staff_oncost_type_id');
            $table->timestamps();
        });

        Schema::create('staff_oncost_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::table('staff_types', function (Blueprint $table) {
            $table->string('special_calculation_method')->after('expense_code_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expense_codes', function (Blueprint $table) {
            $table->dropColumn('staff_oncost_type_id');
        });

        Schema::table('fpa_staff_pay_rates', function (Blueprint $table) {
            $table->integer('scheme_id')->after('pay_rate');
            $table->dropColumn('area_id');
        });

        Schema::table('schemes', function (Blueprint $table) {
            $table->dropColumn('accounting_type');
            $table->dropColumn('contract_care_hours_total');
            $table->dropColumn('contract_domestic_hours_total');
            $table->dropColumn('area_id');
        });

        Schema::dropIfExists('schemes_areas');

        Schema::table('schemes_flats', function (Blueprint $table) {
            $table->dropColumn('lease_type');
            $table->dropColumn('assisted_living_hours_per_week');
        });

        Schema::dropIfExists('staff_oncost_percentages');

        Schema::dropIfExists('staff_oncost_types');

        Schema::table('staff_types', function (Blueprint $table) {
            $table->dropColumn('special_calculation_method');
        });
    }
}
