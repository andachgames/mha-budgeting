<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSplitOfOtherExpense extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fpa_annual_spend_split', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('percentage', 8, 2);
            $table->integer('business_stream_id');
            $table->integer('expense_code_id');
            $table->integer('scheme_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fpa_annual_spend_split');
    }
}
