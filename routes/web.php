<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/budgets', 'HomeController@budgets')->name('budgets');
Route::get('/budget/{id}/{businessStream?}', 'HomeController@budget')->name('budget');
Route::get('/input', 'HomeController@input')->name('input');
Route::get('/phasedbudget/{id}/{businessStream?}', 'HomeController@phasedBudget')->name('phasedbudget');
Route::get('/submit', 'HomeController@submit')->name('submit');
Route::post('/submit/post', 'HomeController@submitPost')->name('submitpost');
Route::get('/staffcosts/{schemeID?}', 'HomeController@staffCosts')->name('staffcosts');

Route::get('/input/annual-spend', 'HomeController@inputAnnualSpend')->name('input-annual-spend');
Route::get('/input/override-staff-pay', 'HomeController@inputOverrideStaffPay')->name('input-override-staff-pay');
Route::get('/input/staff-hours', 'HomeController@inputStaffHours')->name('input-staff-hours');
Route::get('/input/staff-streams', 'HomeController@inputStaffStreams')->name('input-staff-streams');
Route::get('/input/weekly-care-hours', 'HomeController@inputWeeklyCareHours')->name('input-weekly-care-hours');

Route::post('/input/annual-spend/post', 'HomeController@inputAnnualSpendPost')->name('input-annual-spend-post');
Route::post('/input/override-staff-pay/post', 'HomeController@inputOverrideStaffPayPost')->name('input-override-staff-pay-post');
Route::post('/input/staff-hours/post', 'HomeController@inputStaffHoursPost')->name('input-staff-hours-post');
Route::post('/input/staff-streams/post', 'HomeController@inputStaffStreamsPost')->name('input-staff-streams-post');
Route::post('/input/weekly-care-hours/post', 'HomeController@inputWeeklyCareHoursPost')->name('input-weekly-care-hours-post');

Auth::routes();