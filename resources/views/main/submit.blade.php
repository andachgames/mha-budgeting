@extends('template')

@section('content')
<div class="row">
    <div class="col-12">
        <h1>Submit Budget</h1>
    </div>

    @if ($isOK)
        <div class="col-12">You have submitted all necessary information to this Budget. You can submit it below. Please note that once a budget is submitted, it creates an audit log. 

        {{ Form::open(['route' => 'submitpost', 'method' => 'post']) }}
            <p>
                {{ Form::hidden('budgetID', $budget->id) }}
                {{ Form::submit('Submit This Budget', ['class' => 'form-control btn btn-success']) }}
            </p>
        {{ Form::close() }}
        </div>
    @else 
        <p>You have not yet entered all the necessary information for this budget. Please check the <a href="{{ route('input') }}">input page</a> to see what you have missed, then come back here. </p>
    @endif
</div>
@endsection
