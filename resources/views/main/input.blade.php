@extends('template')

@section('content')
<div class="row">
    <div class="col-12">
        <h1>Input</h1>

        <div class="alert alert-info" role="alert">
            You have not signed off a draft budget yet. You will need to enter information in all below areas before you can submit your budget. 
        </div>

        <div class="row">
            <div class="col-3">
                <div class="card">
                    <div class="card-header"><a href="{{ route('input-annual-spend') }}">Annual Spend</a></div>
                    <div class="card-body">Enter annual spend</div>
                    <div class="card-footer @if ($ok['annual_spend']) bg-success @else bg-danger @endif">
                        @if ($ok['annual_spend']) Submitted Successfully @else Not Yet Submitted @endif
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card">
                    <div class="card-header"><a href="{{ route('input-staff-hours') }}">Staff Hours</a></div>
                    <div class="card-body">Enter annual care hours</div>
                    <div class="card-footer @if ($ok['staff_hours']) bg-success @else bg-danger @endif">
                        @if ($ok['staff_hours']) Submitted Successfully @else Not Yet Submitted @endif
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card">
                    <div class="card-header"><a href="{{ route('input-staff-streams') }}">Staff Streams</a></div>
                    <div class="card-body">Enter annual care hours</div>
                    <div class="card-footer @if ($ok['staff_streams']) bg-success @else bg-danger @endif">
                        @if ($ok['staff_streams']) Submitted Successfully @else Not Yet Submitted @endif
                    </div>
                </div>
            </div>
            <div class="col-3">
                <div class="card">
                    <div class="card-header"><a href="{{ route('input-weekly-care-hours') }}">Weekly Care Hours</a></div>
                    <div class="card-body">Enter annual care hours</div>
                    <div class="card-footer @if ($ok['weekly_care_hours']) bg-success @else bg-danger @endif">
                        @if ($ok['weekly_care_hours']) Submitted Successfully @else Not Yet Submitted @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
