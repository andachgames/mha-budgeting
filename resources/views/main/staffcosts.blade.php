@extends('template')

@section('content')
<div class="row">
    <div class="col-12">
        <h1>Staff Costs at {{ $scheme->name }}</h1>

        <div class="alert alert-info" role="alert">
            The below page shows you a summary of staff costs to be used in your scheme. The "base pay" below is the advertised pay per hour to the employee. Then, the finance team have calculated "oncosts", including National Insurance, Pension, etc. based on historical rates in your scheme. This in turn gives an "adjusted pay" per hour.  
        </div>

        <div class="row">
            <div class="col-3">Staff Type</div>
            <div class="col-2">Base Pay/Hour</div>
            <div class="col-1">NI</div>
            <div class="col-1">Pension</div>
            <div class="col-1">Holiday</div>
            <div class="col-1">Sickness</div>
            <div class="col-1">Training</div>
            <div class="col-2">Adj. Pay per Hour</div>
            @foreach ($staffTypes as $staffType)
                <div class="col-3">{{ $staffType->name }}</div>
                <div class="col-2">&pound;{{ $staffType->getFormattedPayPerHour($scheme->id) }}</div>
                <div class="col-1">{{ $staffType->getOncost(1, $scheme->id) }}%</div>
                <div class="col-1">{{ $staffType->getOncost(2, $scheme->id) }}%</div>
                <div class="col-1">{{ $staffType->getOncost(3, $scheme->id) }}%</div>
                <div class="col-1">{{ $staffType->getOncost(4, $scheme->id) }}%</div>
                <div class="col-1">{{ $staffType->getOncost(5, $scheme->id) }}%</div>
                <div class="col-2">&pound;{{ $staffType->getFormattedAdjustedPayPerHour($scheme->id) }}</div>
            @endforeach
        </div>
    </div>
</div>
@endsection
