@extends('template')

@section('content')
<div class="row">
    <div class="col-12">
        <h1>View Budget</h1>
    </div>

    <div class="col-12 alert alert-info" role="alert">
        This is a very basic summary of your budget showing total income and expense per expense code. This screen is currently showing your budget for <b>@if ($businessStreamID) {{ $stream->name }} @else All Business Streams @endif</b>. This can be changed by clicking below:

        <ul>
        	<li><a href="{{ route('budget', $budget->id) }}">All Business Streams</a></li>
        	@foreach ($streams as $stream)
        		<li><a href="{{ route('budget', [$budget->id, $stream->id]) }}">{{ $stream->name }}</a></li>
        	@endforeach
        </ul>
    </div>

    <div class="row">
    	<div class="col-4">Expense Code</div>
    	<div class="col-2">Income</div>
    	<div class="col-2">Staff</div>
    	<div class="col-2">Annual Spend</div>
    	<div class="col-2">Total</div>

    	@foreach ($expenseCodes as $expenseCode)
    		<div class="col-4">{{ $expenseCode->name }}</div>
    		<div class="col-2">{{ $budget->totalIncome($expenseCode->id, $businessStreamID) }}</div>
    		<div class="col-2">{{ $budget->totalStaffCosts($expenseCode->id, $businessStreamID) }}</div>
    		<div class="col-2">{{ $budget->totalOtherCosts($expenseCode->id, $businessStreamID) }}</div>
    		<div class="col-2">{{ $budget->total($expenseCode->id, $businessStreamID) }}</div>
    	@endforeach
</div>
@endsection
