@extends('template')

@section('content')
<div class="row">
    <div class="col-12">
        <h1>View All Budgets</h1>

        <div class="row">
            <div class="col-1">ID</div>
            <div class="col-5">Scheme</div>
            <div class="col-2">Status</div>
            <div class="col-2">Created Date</div>
            <div class="col-2">Updated Date</div>
            @foreach ($budgets as $budget)
                <div class="col-1"><a href="{{ route('budget', $budget->id) }}">{{ $budget->id }} (view)</a></div>
                <div class="col-5">{{ $budget->scheme->name }}</div>
                <div class="col-2">{{ $budget->status }}</div>
                <div class="col-2">{{ $budget->created_at }}</div>
                <div class="col-2">{{ $budget->updated_at }}</div>
            @endforeach
        </div>
    </div>
</div>
@endsection
