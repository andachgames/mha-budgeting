@extends('template')

@section('content')
<div class="row">
    <div class="col-12">
        <h1>View Phased Budget</h1>
    </div>

    <div class="col-12 alert alert-info" role="alert">
        This is your phased budget.


        <ul>
        	<li><a href="{{ route('phasedbudget', $budget->id) }}">All Business Streams</a></li>
        	@foreach ($streams as $stream)
        		<li><a href="{{ route('phasedbudget', [$budget->id, $stream->id]) }}">{{ $stream->name }}</a></li>
        	@endforeach
        </ul>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th scope="col">Expense Code</th>
                <th scope="col">April</th>
                <th scope="col">May</th>
                <th scope="col">June</th>
                <th scope="col">July</th>
                <th scope="col">August</th>
                <th scope="col">September</th>
                <th scope="col">October</th>
                <th scope="col">November</th>
                <th scope="col">December</th>
                <th scope="col">January</th>
                <th scope="col">February</th>
                <th scope="col">March</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($expenseCodes as $expenseCode)
                <tr>
                    <th scope="row">{{ $expenseCode->name }}</th>
                    @foreach ($phasedBudget[$expenseCode->id] as $array)
                    {{ dd($array) }}
                        @foreach ($array as $amount)
                            <td>&pound;{{ $amount }}</td>
                        @endforeach
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection
