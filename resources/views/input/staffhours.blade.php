@extends('template')

@section('content')
@include('input.menu')
<div class="row">
    <div class="col-12">
        <h1>Input - Staff Hours</h1>

        <div class="alert alert-info" role="alert">
            Please enter the estimated weekly number of hours for each category of staff member. The <a href="{{ route('staffcosts') }}">detailed calculation of staff costs</a> is available to show how the adjusted pay is calculated. Adjusted pay takes into account national insurance, pension, etc.
        </div>

        {{ Form::open(['route' => 'input-staff-hours-post', 'method' => 'post']) }}

        <div class="row">
            <div class="col-2">Staff Type</div>
            <div class="col-4">Notes</div>
            <div class="col-2">Basic Pay Rate (Hourly)</div>
            <div class="col-2">Adjusted Pay Rate (Hourly)</div>
            <div class="col-2">Estimate</div>

            @foreach ($staffTypes as $staffType)
                <div class="col-2">{{ $staffType->name }}</div>
                <div class="col-4">{{ $staffType->instructions_hours }}</div>
                <div class="col-2">&pound;{{ $staffType->getFormattedPayPerHour($budget->scheme_id) }}</div>
                <div class="col-2">&pound;{{ $staffType->getFormattedAdjustedPayPerHour($budget->scheme_id) }}</div>
                <div class="col-2">{{ $staffType->getHoursFormInput($currentEstimate[$staffType->id]) }}</div>
            @endforeach

            <div class="col-12">{{ Form::submit('Update Staff Hours Estimates', ['class' => 'form-control btn btn-success']) }}</div>
        </div>

        {{ Form::close() }}
    </div>
</div>
@endsection
