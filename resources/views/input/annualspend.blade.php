@extends('template')

@section('content')
@include('input.menu')

<div class="row">
    <div class="col-12">
        <h1>Input - Annual Spend data</h1>

        <div class="alert alert-info" role="alert">
            Please enter your estimated annual spend (for the 19/20 year), in each of the given categories. To help you, we have included:

            <ul>
                <li>Your prior year (17/18) spend</li>
                <li>Your current year (18/19) budget</li>
                <li>Your current year (18/19) annualised spend so far</li>
            </ul>
        </div>

        {{ Form::open(['route' => 'input-annual-spend-post', 'method' => 'post']) }}

        <div class="row">
            <div class="col-4">Expense Category</div>
            <div class="col-2">17/18 Spend</div>
            <div class="col-2">18/19 Budget</div>
            <div class="col-2">18/19 Annualised</div>

            @foreach ($expenseCodes as $expenseCode)
                <div class="col-4">{{ $expenseCode->name }}</div>
                <div class="col-2">{{ $expenseCode->getActualSpend('actual') }}</div>
                <div class="col-2">{{ $expenseCode->getActualSpend('budget') }}</div>
                <div class="col-2">{{ $expenseCode->getActualSpend('annualised') }}</div>
                <div class="col-2">{{ Form::text('spend['.$expenseCode->id.']', $currentEstimate[$expenseCode->id], ['class' => 'form-control']) }}</div>
            @endforeach

            <div class="col-12">{{ Form::submit('Update Annual Spend Estimates', ['class' => 'form-control btn btn-success']) }}</div>
        </div>

        {{ Form::close() }}
    </div>
</div>
@endsection
