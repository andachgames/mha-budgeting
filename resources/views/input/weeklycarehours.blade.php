@extends('template')

@section('content')
@include('input.menu')
<div class="row">
    <div class="col-12">
        <h1>Input - Weekly Care Hours</h1>

        <div class="alert alert-info" role="alert">
            Please enter the estimated weekly number of care and domestic hours for each flat. 
        </div>

        {{ Form::open(['route' => 'input-weekly-care-hours-post', 'method' => 'post']) }}

        <div class="row">
            <div class="col-4">Flat</div>
            <div class="col-2">Care Hours</div>
            <div class="col-2">Charge for Care Hours</div>
            <div class="col-2">Domestic Hours</div>
            <div class="col-2">Charge for Domestic Hours</div>
        </div>

        @foreach ($flats as $flat)
    	<div class="row">
            <div class="col-4">{{ $flat->name }}</div>
            <div class="col-2">{{ Form::text('care['.$flat->id.']', $currentEstimate[$flat->id]['care'], ['class' => 'form-control']) }}</div>
            <div class="col-2">{{ $chargeForCare }}</div>
            <div class="col-2">{{ Form::text('domestic['.$flat->id.']', $currentEstimate[$flat->id]['domestic'], ['class' => 'form-control']) }}</div>
            <div class="col-2">{{ $chargeForDomestic }}</div>
        </div>
        @endforeach

        <div class="col-12">{{ Form::submit('Update Staff Split between Business Streams', ['class' => 'form-control btn btn-success']) }}</div>

        {{ Form::close() }}
    </div>
</div>
@endsection
