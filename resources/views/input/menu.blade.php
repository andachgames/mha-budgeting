<ul class="nav nav-tabs">
  <li class="nav-item">
    <a class="nav-link @if(Route::is('input-annual-spend')) active @endif" href="{{ route('input-annual-spend') }}">Annual Spend Estimates</a>
  </li>
  <li class="nav-item">
    <a class="nav-link @if(Route::is('input-override-staff-pay')) active @endif" href="{{ route('input-override-staff-pay') }}">Override Staff Pay</a>
  </li>
  <li class="nav-item">
    <a class="nav-link @if(Route::is('input-staff-hours')) active @endif" href="{{ route('input-staff-hours') }}">Staff Hours</a>
  </li>
  <li class="nav-item">
    <a class="nav-link @if(Route::is('input-staff-streams')) active @endif" href="{{ route('input-staff-streams') }}">Staff Streams</a>
  </li>
  <li class="nav-item">
    <a class="nav-link @if(Route::is('input-weekly-care-hours')) active @endif" href="{{ route('input-weekly-care-hours') }}">Weekly Care Hours</a>
  </li>
</ul>