@extends('template')

@section('content')
@include('input.menu')
<div class="row">
    <div class="col-12">
        <h1>Input - Staff Streams</h1>

        <div class="alert alert-info" role="alert">
            Please enter your split of staff between business streams.
        </div>

        {{ Form::open(['route' => 'input-staff-streams-post', 'method' => 'post']) }}

        <div class="row">
            <div class="col-2">Staff Type</div>
            <div class="col-3">Notes</div>
            
            @foreach ($businessStreams as $stream)
           	<div class="col-1">{{ $stream->name }}</div>
            @endforeach
        </div>

            @foreach ($staffTypes as $staffType)
        	<div class="row">
                <div class="col-2">{{ $staffType->name }}</div>
                <div class="col-3">{{ $staffType->instructions_streams }}</div>
                @foreach ($businessStreams as $stream)
                	<div class="col-1">{{ $staffType->getStreamFormInput($currentEstimate[$staffType->id][$stream->id], $stream->id) }}</div>
	            @endforeach
            </div>
            @endforeach

            <div class="col-12">{{ Form::submit('Update Staff Split between Business Streams', ['class' => 'form-control btn btn-success']) }}</div>
        </div>

        {{ Form::close() }}
    </div>
</div>
@endsection
