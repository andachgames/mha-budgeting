@extends('template')

@section('content')
@include('input.menu')
<div class="row">
    <div class="col-12">
        <h1>Input - Override Staff Pay</h1>

        <div class="alert alert-info" role="alert">
            Override staff pay here. 
        </div>

        {{ Form::open(['route' => 'input-override-staff-pay-post', 'method' => 'post']) }}

        

        {{ Form::close() }}
    </div>
</div>
@endsection
