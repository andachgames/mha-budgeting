@extends('template')

@section('content')
<div class="row">
    <div class="col-12">
        <h1>MHA Retirement Living Budgeting Program</h1>

        <p>This website has been set up by the FP&A team to speed up and simplify our budgeting process. </p>
        <p>All retirement living managers should have been provided with a username and password to log onto this website. Your username is your four-digit scheme ID (starting with 4). You will not be able to use this website until you log in. </p>
        <p>Once in, you can submit information on five different areas:</p>
        <ul>
        	<li>Annual expected spend against various expense codes.</li>
        	<li>Total budgeted hours worked by staff members.</li>
        	<li>The split of those staff members between your business streams.</li>
        	<li>Any salaries for staff members not on standard pay grades.</li>
        	<li>Total weekly care/domestic hours expected to be sold.</li>
        </ul>
    </div>
</div>
@endsection
