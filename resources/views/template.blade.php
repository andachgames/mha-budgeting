
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>MHA - Retirement Living Budgeting</title>

    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet">

  </head>

  <body>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
      <div class="container">
        <a class="navbar-brand" href="/">MHA Retirement Living Budgeting</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item active">
              <a class="nav-link" href="/">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('input') }}">Input Data</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('budgets') }}">Previous Budgets</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('staffcosts') }}">Staff Costs Info</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="{{ route('submit') }}">Submit Data</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

    @if (Session::has('success'))
    <div class="container">
      <div class="row">
        <div class="col-12">
          <div class="alert alert-success"><b>Success:</b> {!! Session::get('success') !!}</div>
        </div>
      </div>
    </div>
    @endif


    <!-- Page Content -->
    <div class="container">
      @yield('content')
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

  </body>

</html>
